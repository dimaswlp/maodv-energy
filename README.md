# maodv-energy
modified AODV with energy features

Nama : Dimas Widya Liestio Pamungkas<br>
NRP : 05111850010016<br>
Mt.Kul : TD. Desain Audit Jaringan<br>

Improvement pada AODV standard dengan residual energy dengan menambahkan fitur seleksi energy yang dimiliki node.
Pada paper, energy setiap node pada rute AODV yang dipilih dijumlahkan. Rute dengan jumlah residual energy tertinggi dipilih untuk menjaga konektivitas pengiriman data.
Dilakukan penambahan seleksi yaitu menghindari rute yang memiliki node dengan residual energy terendah untuk menghindari putusnya koneksi.